//
//  BaseViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import OpenTok

class BaseViewController: UIViewController {
    
    //MARK: Variable
    lazy var session: OTSession = {
        return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
        }()
    
    lazy var publisher: OTPublisher = {
       let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    
    var subscriber: OTSubscriber?
    var isAudioCall: Bool = false
    
    //MARK: Custom Function
    func doConnect(){
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.connect(withToken: kToken, error: &error)
    }
    
    func doPublish(){
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.publish(publisher, error: &error)
        
        if let pubView = publisher.view {
            pubView.frame = CGRect(x: 0, y: 0, width: kWidht, height: kHeight)
            view.addSubview(pubView)
        }
    }
    
    func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        subscriber = OTSubscriber(stream: stream, delegate: self)
        session.subscribe(subscriber!, error: &error)
    }
    
    func cleanUpSubcriber(){
        subscriber?.view?.removeFromSuperview()
    }
    
    func cleanUpPublisher(){
        publisher.view?.removeFromSuperview()
    }
    
    func processError(_ error: OTError?){
        if let err = error {
            DispatchQueue.main.async {
                let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }
        }
    }

    func push(_ controller: UIViewController){
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func clearSession(){
        cleanUpPublisher()
        cleanUpSubcriber()
    }
    
    func disconnect(){
        var error: OTError?
        session.disconnect(&error)
    }
    
    func backToViewController() {
        self.navigationController?.popViewController(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension BaseViewController: OTSessionDelegate{
    func sessionDidConnect(_ session: OTSession) {
        print("Session Did Connect")
        doPublish()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session Did Disconnect")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if subscriber == nil {
            doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanUpSubcriber()
        }
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("Session DidFailWithError: \(error.localizedDescription)")
    }
}

extension BaseViewController: OTPublisherDelegate{
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher Failed: \(error.localizedDescription)")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Publisher Stream Created")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        print("Publisher Stream Destroyed")
        cleanUpPublisher()
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanUpSubcriber()
        }
    }
}

extension BaseViewController: OTSubscriberDelegate{
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        print("Subscriber Did Connect")
        if let subsView = subscriber?.view, isAudioCall == false {
            subsView.frame = CGRect(x: 0, y: kHeight, width: kWidht, height: kHeight)
            view.addSubview(subsView)
        }
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber DidFailWithError \(error.localizedDescription)")
    }
}
