//
//  BaseButton.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class BaseButton: UIButton {

    func asMainButton(){
        self.layer.cornerRadius = 10
//        self.backgroundColor = UIColor.init(red: 153/255, green: 186/255, blue: 211/255, alpha: 1.0)
        self.setTitleColor(UIColor.white, for: .normal)
    }

}
