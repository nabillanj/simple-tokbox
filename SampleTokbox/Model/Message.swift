//
//  Message.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 10/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

enum MessageType {
    case chat, alert
    
    func color() -> UIColor {
        switch self {
        case .chat: return UIColor(red: 68/255, green: 68/255, blue: 68/255, alpha: 0.6)
        case .alert: return UIColor(red: 255/255, green: 60/255, blue: 50/255, alpha: 0/255)
        }
    }
}

struct Message {
    var text: String!
    var type: MessageType = .chat
    
    init(text: String, type: MessageType) {
        self.text = text
        self.type = type
    }
}
