//
//  MessageViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 11/12/18.
//

import UIKit

class MessageViewController: OTTextChatViewController {
    
    //MARK: Variables
    var textChat: OTTextChat?
    var textMessage = [OTTextMessage]()

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        textChat = OTTextChat()
        textChat?.dataSource = self
        textChat?.alias = "Room Name"
        
        textChatNavigationBar.topItem?.title = textChat?.alias
        tableView.textChatTableViewDelegate = self
        tableView.separatorStyle = .none
        textChatInputView.textField.delegate = self
        
        textChat?.connect(handler: { (signal, connection, error) in
            
            guard error == nil else {
                SVProgressHUD.showError(withStatus: error!.localizedDescription)
                return
            }
            
            if signal == .didConnect {
                print("Text Chat starts")
            }
            else if signal == .didDisconnect {
                print("Text Chat stops")
            }
            
        }) { [unowned self](signal, message, error) in
            
            guard error == nil, let message = message else {
                SVProgressHUD.showError(withStatus: error!.localizedDescription)
                return
            }
            
            self.textMessage.append(message)
            self.tableView.reloadData()
            self.textChatInputView.textField.text = nil
            self.scrollTextChatTableViewToBottom()
        }
        
        textChatInputView.sendButton.addTarget(self, action: #selector(MessageViewController.sendTextMessage), for: .touchUpInside)
    }
    
    @objc func sendTextMessage() {
        textChat?.sendMessage(textChatInputView.textField.text)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension MessageViewController: OTTextChatDataSource {
    public func session(of textChat: OTTextChat!) -> OTAcceleratorSession! {
        return (UIApplication.shared.delegate as? AppDelegate)?.session
    }
}

extension MessageViewController: OTTextChatTableViewDataSource {
    
    func type(of tableView: OTTextChatTableView!) -> OTTextChatViewType {
        return .default
    }
    
    public func textChatTableView(_ tableView: OTTextChatTableView!, numberOfRowsInSection section: Int) -> Int {
        return  textMessage.count
    }
    
    func textChatTableView(_ tableView: OTTextChatTableView!, textMessageItemAt indexPath: IndexPath!) -> OTTextMessage! {
        return textMessage[indexPath.row]
    }
    
    func textChatTableView(_ tableView: OTTextChatTableView!, cellForRowAt indexPath: IndexPath!) -> UITableViewCell! {
        return nil
    }
}

extension MessageViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendTextMessage()
        return true
    }
}
