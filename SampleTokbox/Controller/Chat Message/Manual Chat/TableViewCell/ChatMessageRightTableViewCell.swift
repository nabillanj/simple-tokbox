//
//  ChatMessageRightTableViewCell.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 11/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class ChatMessageRightTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //MARK: Custom Function
    func bind(message: ChatMessage) {
        self.messageLabel.text = message.message
        self.userNameLabel.text = message.account
        self.userNameView.layer.cornerRadius = self.userNameView.frame.size.width / 2
        self.userNameView.clipsToBounds = true
        self.backgroundColor = UIColor.clear
        self.userNameLabel.adjustsFontSizeToFitWidth = true
    }
}
