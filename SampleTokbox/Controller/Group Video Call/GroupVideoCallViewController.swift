//
//  GroupVideoCallViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import OpenTok

protocol GroupCallDelegate: class {
    func didCloseGroupCall(_ vc: GroupVideoCallViewController)
}

class GroupVideoCallViewController: UIViewController {
    
    //MARK: Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var hangUpButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    
    //MARK: Variables
    var subscribers: [OTSubscriber] = []
    lazy var session: OTSession = {
        return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
    }()
    
    lazy var publisher: OTPublisher = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    
    weak var delegate: GroupCallDelegate?
    var isMute = false {
        didSet {
            publisher.publishAudio = isMute
            muteButton.setImage(UIImage(named: isMute ? "mute" : "unmute"), for: .normal)
        }
    }
    
    var switchCamera = false {
        didSet {
            if publisher.cameraPosition == .back {
                publisher.cameraPosition = .front
            } else {
                publisher.cameraPosition = .back
            }
            switchCameraButton.setImage(UIImage(named: isMute ? "switch_camera" : "unswitch-camera"), for: .normal)
            
        }
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        doConnect()
        let nib = UINib(nibName: "GroupVideoCallCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "videoCell")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        disconnect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Custom Function
    func doConnect() {
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.connect(withToken: kToken, error: &error)
    }
    
    func disconnect() {
        var error: OTError?
        session.disconnect(&error)
    }
    
     func doPublish() {
        var error: OTError?
        defer {
            processError(error)
        }
        session.publish(publisher, error: &error)
        
        collectionView?.reloadData()
    }
    
     func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        guard let subscriber = OTSubscriber(stream: stream, delegate: self)
            else {
                print("Error while subscribing")
                return
        }
        session.subscribe(subscriber, error: &error)
        subscribers.append(subscriber)
        collectionView?.reloadData()
    }
    
    fileprivate func cleanupSubscriber(_ stream: OTStream) {
        subscribers = subscribers.filter { $0.stream?.streamId != stream.streamId }
        collectionView?.reloadData()
    }
    
     func processError(_ error: OTError?) {
        if let err = error {
            showAlert(errorStr: err.localizedDescription)
        }
    }
    
    func showAlert(errorStr err: String) {
        DispatchQueue.main.async {
            let controller = UIAlertController(title: "Error", message: err, preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //MARK: Actions
    @IBAction func didClickMuteButton(_ sender: Any) {
        isMute = !isMute
    }
    
    @IBAction func didClickHangUpButton(_ sender: Any) {
        disconnect()
        self.delegate?.didCloseGroupCall(self)
    }
    
    @IBAction func didClickSwitchCameraButton(_ sender: Any) {
        switchCamera = !switchCamera
    }
}

extension GroupVideoCallViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subscribers.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath)
        let videoView: UIView? = {
            if (indexPath.row == 0) {
                return publisher.view
            } else {
                let sub = subscribers[indexPath.row - 1]
                return sub.view
            }
        }()
        
        if let viewToAdd = videoView {
            viewToAdd.frame = cell.bounds
            cell.addSubview(viewToAdd)
        }
        return cell
    }
}

// MARK: - OTSession delegate callbacks
extension GroupVideoCallViewController: OTSessionDelegate {
     func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        doPublish()
    }
    
     func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
     func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        doSubscribe(stream)
    }
    
     func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        cleanupSubscriber(stream)
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
}

// MARK: - OTPublisher delegate callbacks
extension GroupVideoCallViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - OTSubscriber delegate callbacks
extension GroupVideoCallViewController: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        print("Subscriber connected")
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
    
    func subscriberVideoDataReceived(_ subscriber: OTSubscriber) {
    }
}
