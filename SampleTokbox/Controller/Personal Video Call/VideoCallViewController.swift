//
//  VideoCallViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import OpenTok

protocol VideoCallDelegate: class {
    func onCloseVideoCall(_ controller: VideoCallViewController)
}

class VideoCallViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var hangUpButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    
    //MARK: Variables
    weak var delegate: VideoCallDelegate?
    var isMute = false {
        didSet {
            publisher.publishAudio = isMute
            //subscriber?.subscribeToAudio = isMute
            muteButton.setImage(UIImage(named: isMute ? "mute" : "unmute"), for: .normal)

        }
    }
    
    var switchCamera = false {
        didSet {
            if publisher.cameraPosition == .back {
                publisher.cameraPosition = .front
            } else {
                publisher.cameraPosition = .back
            }
            switchCameraButton.setImage(UIImage(named: isMute ? "switch_camera" : "unswitch-camera"), for: .normal)
            
        }
    }

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        doConnect()
        isMute = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disconnect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    @IBAction func didClickMuteButton(_ sender: Any) {
        isMute = !isMute
    }
    
    @IBAction func didClickHangUpButton(_ sender: Any) {
        disconnect()
        delegate?.onCloseVideoCall(self)
    }
    
    @IBAction func didClickSwitchCameraButton(_ sender: Any) {
        switchCamera = !switchCamera
    }
    
}
