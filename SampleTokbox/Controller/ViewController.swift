//
//  ViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import OpenTok

class ViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var voiceCallButton: BaseButton!
    @IBOutlet weak var videoCallButton: BaseButton!
    @IBOutlet weak var broadcasterButton: BaseButton!
    @IBOutlet weak var chatButton: BaseButton!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setup UI
    func setupUI(){
        voiceCallButton.asMainButton()
        videoCallButton.asMainButton()
        broadcasterButton.asMainButton()
        chatButton.asMainButton()
    }
    
    //MARK: Actions
    @IBAction func didClickVoiceCallButton(_ sender: Any) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Voice Call", style: .default, handler: { action in
            let contVoice = VoiceCallViewController(nibName: "VoiceCallViewController", bundle: nil)
            contVoice.delegate = self
            self.push(contVoice)
        }))
        controller.addAction(UIAlertAction(title: "Video Call", style: .default, handler: { action in
            let contVideo = VideoCallViewController(nibName: "VideoCallViewController", bundle: nil)
            contVideo.delegate = self
            self.push(contVideo)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func didClickVideoCallButton(_ sender: Any) {
        let contVideo = GroupVideoCallViewController(nibName: "GroupVideoCallViewController", bundle: nil)
        contVideo.delegate = self
        push(contVideo)
    }
    
    @IBAction func didClickBroadcastButton(_ sender: Any) {
        let vc = UIStoryboard(name: "BroadcastStoryboard", bundle: nil).instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
        vc.delegate = self
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "As Publisher", style: .default, handler: { action in
            vc.isSubscriber = false
            self.push(vc)
        }))
        controller.addAction(UIAlertAction(title: "As Subscriber", style: .default, handler: { action in
            vc.isSubscriber = true
            self.push(vc)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func didClickChatButton(_ sender: Any) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Default Chat", style: .default, handler: { action in
           self.present(MessageViewController(), animated: true, completion: nil)
        }))
        controller.addAction(UIAlertAction(title: "Manual Chat", style: .default, handler: { action in
            let vc = UIStoryboard(name: "ChatStoryboard", bundle: nil).instantiateViewController(withIdentifier: "ChatMessageViewController") as! ChatMessageViewController
            self.push(vc)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
}

extension ViewController: VoiceCallDelegate {
    func onCloseVoiceCall(_ controller: VoiceCallViewController) {
        backToViewController()
    }
}

extension ViewController: VideoCallDelegate {
    func onCloseVideoCall(_ controller: VideoCallViewController) {
        backToViewController()
    }
}

extension ViewController: GroupCallDelegate {
    func didCloseGroupCall(_ vc: GroupVideoCallViewController) {
        backToViewController()
    }
}

extension ViewController: BroadcastDelegate {
    func didClickCloseButton(_ vc: BroadcastViewController) {
        backToViewController()
    }
    
    
}
