//
//  VoiceCallViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

protocol VoiceCallDelegate: class {
    func onCloseVoiceCall(_ controller: VoiceCallViewController)
}

class VoiceCallViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var muteBtn: UIButton!
    
    //MARK: Variables
    var isMute = false {
        didSet {
            publisher.publishAudio = isMute
            //subscriber?.subscribeToAudio = isMute
            muteBtn.setImage(UIImage(named: isMute ? "mute" : "unmute"), for: .normal)
        }
    }
    
    weak var delegate: VoiceCallDelegate?

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        doConnect()
        isMute = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        disconnect()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isAudioCall = true
        isMute = false
        publisher.publishVideo = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: Actions
    @IBAction func didClickMuteButton(_ sender: UIButton) {
        isMute = !isMute
    }
    
    @IBAction func didClickHangUpButton(_ sender: Any) {
        disconnect()
        delegate?.onCloseVoiceCall(self)
    }
}
