//
//  BroadcastViewController.swift
//  SampleTokbox
//
//  Created by nabilla nurjannah on 07/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import OpenTok

protocol BroadcastDelegate: class {
    func didClickCloseButton(_ vc: BroadcastViewController)
}

class BroadcastViewController: UIViewController {
    
    //MARK: Outlet
    @IBOutlet weak var hangUpButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constraintBottomLayout: NSLayoutConstraint!
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet var button: [UIButton]!
    
    
    //MARK: Variables
    weak var delegate: BroadcastDelegate?
    var chatMessageVC: ChatViewController?
    var isSubscriber: Bool = false
    var isMute = false {
        didSet {
            publisher.publishAudio = isMute
            muteButton.setImage(UIImage(named: isMute ? "mute" : "unmute"), for: .normal)
            
        }
    }
    
    var switchCamera = false {
        didSet {
            if publisher.cameraPosition == .back {
                publisher.cameraPosition = .front
            } else {
                publisher.cameraPosition = .back
            }
            switchCameraButton.setImage(UIImage(named: isMute ? "switch_camera" : "unswitch-camera"), for: .normal)
            
        }
    }
    
    var subscribers: [OTSubscriber] = []
    lazy var session: OTSession = {
        return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
    }()
    
    lazy var publisher: OTPublisher = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    
    var isInputing = false {
        didSet {
            if isInputing {
                messageTextView?.becomeFirstResponder()
            } else {
                messageTextView.text = ""
                messageTextView?.resignFirstResponder()
            }
            viewButton?.isHidden = !isInputing
            messageButton?.setImage(UIImage(named: isInputing ? "sent" : "unsent"), for: .normal)
        }
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        doConnect()
        let nib = UINib(nibName: "GroupVideoCallCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "videoCell")
        for btn:UIButton in button {
            btn.isHidden = isSubscriber
        }
        addKeyboardObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        disconnect()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Custom Function
    func doConnect() {
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.connect(withToken: kToken, error: &error)
    }
    
    func disconnect() {
        var error: OTError?
        session.disconnect(&error)
    }
    
    func doPublish() {
        var error: OTError?
        defer {
            processError(error)
        }
        session.publish(publisher, error: &error)
        
        collectionView?.reloadData()
    }
    
    func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        guard let subscriber = OTSubscriber(stream: stream, delegate: self)
            else {
                print("Error while subscribing")
                return
        }
        session.subscribe(subscriber, error: &error)
        subscribers.append(subscriber)
        collectionView?.reloadData()
    }
    
    fileprivate func cleanupSubscriber(_ stream: OTStream) {
        subscribers = subscribers.filter { $0.stream?.streamId != stream.streamId }
        collectionView?.reloadData()
    }
    
    func processError(_ error: OTError?) {
        if let err = error {
            showAlert(errorStr: err.localizedDescription)
        }
    }
    
    func showAlert(errorStr err: String) {
        DispatchQueue.main.async {
            let controller = UIAlertController(title: "Error", message: err, preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func send(text: String) {
        if !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            var error: OTError?
            session.signal(withType: "type", string: text, connection: nil, error: &error)
            print("Session post chat")
            //chatMessageVC?.append(chat: text, fromUid: 0)
            isInputing = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier else {
            return
        }
        
        switch segueId {
        case "VideoVCEmbedChatMessageVC":
            chatMessageVC = segue.destination as? ChatViewController
        default:
            break
        }
    }
    
    //MARK: Actions
    @IBAction func didClickMessageButton(_ sender: Any) {
        isInputing = !isInputing
    }
    @IBAction func didClickMuteButton(_ sender: Any) {
        isMute = !isMute
    }
    
    @IBAction func didClickHangUpButton(_ sender: Any) {
        disconnect()
        delegate?.didClickCloseButton(self)
    }
    
    @IBAction func didClickSwitchCameraButton(_ sender: Any) {
        switchCamera = !switchCamera
    }
    
    @IBAction func didEnterButtonSend(_ sender: Any) {
        send(text: messageTextView.text)
    }
    
    @IBAction func didDoubleTapView(_ sender: UITapGestureRecognizer) {
        isInputing = false
    }
}

extension BroadcastViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //send(text: textField.text!)
        return true
    }
}

extension BroadcastViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return subscribers.count + 1
        if isSubscriber == false {
            return 1
        }else {
            return subscribers.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath)
        let videoView: UIView? = {
            // if (indexPath.row == 0) {
            if isSubscriber == false {
                return publisher.view
            } else {
                let sub = subscribers[indexPath.row]
                return sub.view
            }
            //            } else {
            //                if isSubscriber == true {
            //                    let sub = subscribers[indexPath.row - 1]
            //                    return sub.view
            //                }else {
            //                    return UIView()
            //                }
            //            }
        }()
        
        if let viewToAdd = videoView {
            viewToAdd.frame = cell.bounds
            cell.addSubview(viewToAdd)
        }
        return cell
    }
}

// MARK: - OTSession delegate callbacks
extension BroadcastViewController: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        if isSubscriber == false {
            doPublish()
        }
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if isSubscriber == true {
            doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        cleanupSubscriber(stream)
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
    
    func session(_ session: OTSession, receivedSignalType type: String?, from connection: OTConnection?, with string: String?) {
        print("Session get chat")
        chatMessageVC?.append(chat: string!, fromUid: 0)
    }
}

// MARK: - OTPublisher delegate callbacks
extension BroadcastViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - OTSubscriber delegate callbacks
extension BroadcastViewController: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        print("Subscriber connected")
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
}

private extension BroadcastViewController {
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { [weak self] notify in
            guard let strongSelf = self, let userInfo = (notify as NSNotification).userInfo,
                let keyBoardBoundsValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue,
                let durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber else {
                    return
            }
            
            let keyBoardBounds = keyBoardBoundsValue.cgRectValue
            let duration = durationValue.doubleValue
            let deltaY = keyBoardBounds.size.height
            
            if duration > 0 {
                var optionsInt: UInt = 0
                if let optionsValue = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    optionsInt = optionsValue.uintValue
                }
                let options = UIViewAnimationOptions(rawValue: optionsInt)
                
                UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
                    strongSelf.constraintBottomLayout.constant = deltaY
                    strongSelf.view?.layoutIfNeeded()
                }, completion: nil)
                
            } else {
                strongSelf.constraintBottomLayout.constant = deltaY
            }
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak self] notify in
            guard let strongSelf = self else {
                return
            }
            
            let duration: Double
            if let userInfo = (notify as NSNotification).userInfo, let durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                duration = durationValue.doubleValue
            } else {
                duration = 0
            }
            
            if duration > 0 {
                var optionsInt: UInt = 0
                if let userInfo = (notify as NSNotification).userInfo, let optionsValue = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    optionsInt = optionsValue.uintValue
                }
                let options = UIViewAnimationOptions(rawValue: optionsInt)
                
                UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
                    strongSelf.constraintBottomLayout.constant = 0
                    strongSelf.view?.layoutIfNeeded()
                }, completion: nil)
                
            } else {
                strongSelf.constraintBottomLayout.constant = 0
            }
        }
    }
}
